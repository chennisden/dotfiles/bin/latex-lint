for path in tests/*.in; do
	NAME=${path%.*}
	DIFF=$(./latex-lint $NAME.in | diff - $NAME.out)
	if [ -z "$DIFF" ]
	then
		echo Test $NAME succeeded.
	else
		echo Test $NAME failed, with diff
		echo $DIFF
	fi
done
